 * General:

>Necronomicon Warrior truesight radius increased from 1000 to 1200
>Courier Burst cooldown reduced from 90 to 60
>Courier Burst duration increased from 4 to 60
>Courier Burst movement speed increased from 650 to 2250
>Courier Burst damage absorb increased from 250 to 500
>Roshan's Bash chance increased from 15% to 17%
>Roshan's Bash duration increased from 1.65 to 1.7 seconds
>Neutral Wolves critical damage increased from 200% to 300%
>Alpha Wolf command aura increased from 30% to 40%
>Satyr Trickster Purge range increased from 350 to 750
>Forest Troll Heal cast point improved to 0.25 seconds and heal increased to 25
>Forest Troll mana aura mana regen increased from 2 to 6

 * Heroes:
      ANTI-MAGE
>Base armor increased by 1
>Base damage increased by 10
>Base attack time improved to 1.3 seconds
>STR gain increased to 2.2
>Turn rate improved to 1.0
>Mana break damage percentage increased from 60% to 100%
>Mana break mana per hit increased to 30/60/90/120
>Blink cooldown rebalanced to 9/7/5/3 seconds
>Blink manacost removed
>Blink range rebalanced to 2400
>Spell shield increased to 70/80/90/100%
>Mana Void radius increased from 500 to 1000
>Mana Void manacost rebalanced from 125/200/275 to 125
>Mana Void damage per missing mana increased to 1.0/1.5/2.0
>Blink cast point changed to 0.4/0.4/0.4/0.0

      	   AXE
>Berserker's call manacost removed
>Berserker's call radius increased to 400
>Berserker's call extra armor increased to 60
>Battle Hunger slow rebalanced to -10/-20/-30/-40%
>Counter Helix damage rebalaned to 225/325/425/525
>Counter Helix cooldown removed
>Counter Helix trigger chance increased to 50/60/70/80%
>Counter Helix radius increased to 750/850/950/1050
>Culling Blade manacost removed
>Culling Blade kill threshold increased to 300/450/625
>Culling Blade Aghanim's Scepter kill threshold increased to 650/800/950

	  BLOODSEEKER
>Movespeed increased from 290 to 350
>Bloodrage damage increase rebalanced to 40/60/80/100
>Bloodrage hero kill heal percentage rebalanced to 25/25/25/50%
>Bloodrage creep kill heal percentage rebalanced to 25/25/25/50%
>Bloodbath delay improved to 1.7 second from 2.6 seconds
>Thirst sight threshold increased to 40%
>Thirst bonus movespeed increased to 40/50/60/70%
>Rupture cooldown decreased to 60
>Rupture manacost rebalanced to 150
>Rupture damage percentage increased to 40/60/80%
>Rupture cast point improved from 0.6 to 0.35 seconds

	 DROW RANGER
>Base Attack Time improved to 1.5 seconds
>STR gain increased to 2.4
>AGI gain increased to 2.9
>Turn rate increased to 1.0
>Frost Arrows hero duration increased to 2.0 seconds
>Frost Arrows movement speed slow increased to -30/-45/-60/-75%
>Gust manacost rebalanced to 90/80/70/60
>Precision Aura bonus damage increased to 36/46/56/76%
>Marksmanship bonus agility increased to 60/80/100
>Marksmanship disable AoE improved to 190
>Nighttime vision increased from 800 to 1800
>Movespeed increased from 300 to 305

	 EARTHSHAKER
>Enchant Totem bonus damage increased to 300/400/500/600%
>Base STR increased from 22 to 27
>STR gain increased to 3.4

	 JUGGERNAUT
>Armor increased by 2
>Damage increased by 20
>Base Attack Time improved from 1.4 to 1.2 seconds
>Base HP regen increased from 0.75 to 3.75
>Movespeed increased to 325
>STR gain increased to 2.9
>INT gain increased to 2.4
>AGI gain increased to 3.0
>Turn rate improved to 1.0
>Blade Dance critical damage increased to 250%
>Blade Dance critical chance increased to 40/50/60/70%
>Blade Fury manacost rescaled to 110/70/30/0
>Blade Fury radius increased to 300
>Healing ward heal increased to 7/8/9/10%
>Omnislash Aghanim's scepter cooldown improved to 40
>Omnislash Aghanim's scepter jumps increased to 9/12/15

	  MIRANA
>Armor increased by 2
>Magic resistance increased to 35%
>Base attack damage increased by 10
>STR gain increased from 1.85 to 2.1
>AGI gain increased from 2.75 to 2.85
>INT gain increased from 1.65 to 2.0
>Movespeed increased from 300 to 310
>Turn rate improved from 0.4 to 1.0

	  LINA
>Fiery Soul duration increased to 18 seconds
>Fiery Soul max stacks increased to 9

	  LION
>Impale cooldown rebalanced from 12 tp 12/11/10/8 seconds
>Impale damage increased from 80/140/200/260 to 140/220/300/380
>Hex manacost rebalanced from 125/150/175/200 to 125
>Mana Drain cooldown improved from 20/15/10/5 to 15/10/5/0 seconds
>Mana Drain manacost removed
>Finger of Death manacost rebalanced from 200/420/650 to 200/420/420
>Finger of Death Aghanim's Scepter damage increased from 725/875/1025 to 875/1025/1325
>Finger of Death Aghanim's Scepter manacost reduced to 125
>Finger of Death Aghanim's Scepter cooldown reduced from 100/60/20 to 60/20/10
>Finger of Death Aghanim's Scepter splash radius increased from 200 to 350
>Armor increased by 2
>Damage increased by 10
>Base Attack Time improved to 1.5 seconds
>Projectile speed increased from 900 to 1100
>STR gain increased from 1.7 to 2.4
>AGI gain increased from 1.5 to 2.2
>Movespeed increased from 290 to 310
>Turn rate improved from 0.5 to 1.0
>Health Regen now 4.0

       	  MORPHLING
>Adaptive Strike maximum damage multiplier increased to 0.5/1.0/1.5/3.0
>Morph bonus stats increased to 9/16/25/36 (for both stats)

       	  NEVERMORE
>Necromastery damage per soul increased to 4
>Presence of the Dark Lord armor reduction increased from 3/4/5/6 to 5/6/7/8

	  PHANTOM LANCER
>Dopplewalk cooldown increased from 25/20/15/10 to 110

	  PUCK
>Waning Rift silence duration increased to 3/4/5/6

	  PUDGE
>Flesh Heap magic resistance increased to 10/15/20/25%
>Flesh Heap strength bonus increased to 2/2.5/3/3.5
>Flesh Heap range improved from 450 to 1150
>Meat Hook cooldown improved from 14/13/12/11 to 14/7/7/6
>Meat Hook speed rebalanced from 1600 to 1600/1600/1600/2000
>Meat Hook manacost improved to 100/50/25/0
>Rot radius increased to 450
>Dismember channel time increased by 0.05 seconds
>Dismember cast range decreased from 160 to 128
>Movespeed increased to 300

	  SHADOW SHAMAN
>Serpent Wards ward count increased from 10 to 12

     	  RAZOR
>Unstable Current movespeed bonus increased from 4/8/12/16% to 5/10/15/20%     	  
>Eye of the Storm Aghanim's scepter strike interval improved to 0.55/0.45/0.15
>Turn rate reduced to 0.3

     	  WRAITH KING
>Wraithfire Blast range now global
>Wraithfire Blast projectile speed decreased to 650
>Vampiric Aura lifesteal rebalanced to 30/60/90/120%
>Mortal Strike critical chance increased to 15/25/35/45%
>Reincarnation cooldown improved to 60/30/0
>Reincarnation move slow rebalanced to -75/-75/-20%
>Reincarnation slow duration rebalanced to 5/5/3 seconds
>Armor increased by 2
>Damage increased by 20-30
>Base Attack Time improved to 1.5 seconds
>Base STR increased from 22 to 28
>STR gain increased from 2.9 to 3.4
>Base AGI increased from 18 to 32
>AGI gain increased from 1.7 to 2.85
>Movespeed increased from 300 to 315
>Turn rate improved from 0.4 to 1.0

	  DEATH PROPHET
>Crypt Swarm manacost improved from 105/120/140/165 to 100

	  SVEN
>Storm Bolt manacost rebalanced to 140/120/100/80
>Great Cleave increased to 50/65/80/95%
>Warcry range now global
>Warcry bonus movespeed increased to 12/14/16/18%
>God's Strength cooldown rescaled to 80/70/60
>God's Strength duration rescaled to 25/35/45
>God's Strength manacost rescaled to 100
>God's Strength bonus damage increased to 200/300/400%
>God's Strength Aghanim's scepter buff is now global
>God's Strength Aghanim's scepter bonus damage increased to 80/100/120%

       	 STORM SPIRIT
>Static Remnant manacost rebalanced to 70
>Static Remnant damage increased to 140/240/340/440
>Electric Vortec self-slow duration decreased to 3/3/2/1 seconds
>Overload damage rebalanced to 30/60/90/120
>Overload movespeed slow increased to 80%
>Ball Lightning move speed increased to 1875/2500/3500

	  SAND KING
>Epicenter slow increased from 30% to 30/60/90%
>Epicenter attack speed slow increased from 30 to 400

      	TINY
>Avalance manacost rebalanced to 120/100/80/60
>Toss manacost rebalanced to 120/100/80/60
>Grow bonus damage increased to 100/200/300
>Grow attack speed changed from -20/-35/-50 to +20/+35/+50
>Grow Aghanim's Scepter cleave increased to 80%
>INT gain increased from 1.6 to 2.2
>AGI gain increased from 0.9 to 1.7
      
      ZEUS
>Arc Lightning damage increased to 100/175/250/325
>Arc Lightning manacost decreased to 50
>Arc Lightning cooldown rebalanced to 1.75/1.5/1.25/1.0
>Lightning Bolt cooldown rebalanced to 6.0/5.5/5.0/4.5
>Thundergod's Wrath damage increased to 350/475/600
>Thundergod's Wrath Aghanim's scepter damage increased to 540/640/750
>Attack range increased to 500
      
      SLARDAR
>Sprint cooldown decreased to 20
>Sprint bonus damage removed
>Spirnt bonus movespeed rebalanced to 20/30/40/50%
>Slithereen Crush manacost reduced to 80
>Slithereen Crush radius increased to 450
>Bash chance increased to 25/30/35/40
>Bash damage increased to 100/120/140/160
>Amplify damage armor reduction increased to -20/-40/-60
>Amplify damage duration rebalanced to 25/35/45

      TIDEHUNTER
>Ravage now pierces spell immunity
>Ravage radius doubled (2050)
>Ravage manacost rebalanced from 150/225/325 to 150
>Kraken Shell damage reduction increased to 30/40/50/60
>Anchor Smash manacost reduced to 20
>Anchor Smash damage rebalanced to 175/225/375/425

      VENGEFUL SPIRIT
>Magic Missile stun duration increased to 2/2.75/3.25/4 seconds
>Magic Missile damage is now pure
>Magic Missile manacost improved from 110/120/130/140 to 100
>Vengeance aura increased to 16/24/32/40%
>Wave of Terror armor reduction increased to -6/-8/-10/-12
>Attack damage increased by 10
>STR gain increased from 2.6 to 2.8
>Base INT increased from 15 to 19
>INT gain increased from 1.75 to 2.2
>AGI gain increased from 2.8 to 3.2
>Armor increased by 2

      CRYSTAL MAIDEN
>Arcane Aura mana regen increased from 1/1.5/2/2.5 to 4/6/8/10
>Base armor increased by 3
>Base magic resistance reduced to 15%
>Arcane Aura now has a 15% chance to drop an arcana rarity item when used

	LICH
>Removed from the game
>Sacrifice cooldown improved to 30/25/20/15

	RIKI
>Backstab angle increased to 360 degrees
	  
	  WITCH DOCTOR
>Paralyzing Cask manacost reduced from 110/120/130/140 to 100
>Voodoo Restoration activation manacost removed
>Voodoo Restoration mana per second rebalanced from 8/12/16/20 to 10
>Voodoo Restoration radius increased from 500 to 500/600/700/800

	  ENIGMA
>Black Hole cooldown rebalanced from 200/190/180 to 180/120/80
>Black Hole pull speed increased from 40 to 100

	  WINDRANGER
>Powershot damage now pure
>Powershot damage increased from 120/200/280/360 to 120/240/360/480
>Powershot damage and speed reductions removed
>Powershot arrow speed increased from 3000 to 3000/4000/5000/6000
>Powershot tree destroy radius increased from 75 to 75/75/175/275
>Windranger is now an agility hero
>Stats reworked:
[Base Stat] : [Stat Gain]
STR: 15 : 2.7
AGI: 24 : 2.7
INT: 19 : 2.0
>Movespeed increased from 295 to 305
>Turn rate improved from 0.6 to 0.9

	TINKER
>Laser damage rebalanced to 100/200/300/400
>Heat Seeking Missile cooldown decreased to 14 seconds
>Heat Seeking Missile damage increased to 100/200/300/400
>March of the Machines machines per seconds increased from 24 to 32
>Rearm channel time rebalanced to 3/2/0.5 seconds
>Rearm cast point changed to 0/0/0.5 (total time to cast still 3/2/1)
>Rearm manacost rebalanced to 350/150/0
>Model size increased by 10%

       SNIPER
>Shrapnel manacost removed
>Added +1 armor
>Headshot chance rebalanced to 30/40/50/60%
>Headshot slow duration reduced from 0.5 to 0.4 seconds
>Take Aim bonus range increased to 200/300/400/500
>Assassinate manacost rescaled to 175

	NECROPHOS
>Death Pulse manacost decreased to 75
>Death Pulse radius increased to 475
>Death Pulse heal increased to 75/125/200/275
>Heartstopper's Aura damage increased to -1.0/-1.2/-1.5/-1.8
>Reaper's Scythe added respawn time rebalanced to 10/20/30
>Sadist mana regen rebalanced from 2/4/6/10 to 4/6/10/14
>Sadist health regen rebalanced from 1/2/3/4 to 2/4/6/8

	WARLOCK
>Fatal Bonds removed for ceremonial reasons
>Upheaval now has 7 levels
>Upheaval cooldown is now 50/46/42/38/34/30/26
>Upheaval manacost reduced from 100/110/120/130 to 100
>Upheaval AoE rebalanced from 650 to 650/650/650/650/690/530/580
>Upheaval slow rate now 7/14/21/28/28/28%
>Upheaval max slow now 84/84/84/84/100/100/100
>Chaotic Offering now has 4 levels
>Chaotic Offering can be leveled at levels 6/11/16/21
>Chaotic Offering cooldown now 165/165/165/105
>Chaotic Offering cast point now 0.5/0.5/0.5/0.4
>Chaotic Offering manacost now 200/300/400/500

       BEASTMASTER
>Primal Roar damage now pure
>Primal Roar target damage increased to 800/1400/2000
>Primal Roar side damage increased to 800/1400/2000

	QUEEN OF PAIN
>Sonic Wave cooldown decreased to 60/40/20
>Sonic Wave Aghanim's Scepter cooldown decreased to 10
>Portrait now focuses on Queen of Pain's chest

        FACELESS VOID
>Backtrack rebalanced to 25/50/75/100%
>Removed from the game

	PUGNA
>Life Drain manacost rescaled to 150/75/0
>STR gain increased to 1.8

        PHANTOM ASSASSIN
>Stifling Dagger manacost decreased to 15/10/5/0
>Stifling Dagger damage increased to 100/200/300/400
>Stifling Dagger damage type is now physical
>Stifling Dagger move slow increased to -100%
>Stifling Dagger duration increased to 3/4/5/6
>Blink Strike duration increaded to 15
>Blink Strike now grants maximum attack speed
>Blink Strike attack count increased to 15
>Blink Strike range increased to 2000
>Blur evasion increased to 70/80/90/100%
>Coup de Grace critical damage increased to 350/450/650%
>Coup de Grace critical chance increased to 60/70/80%
>Removed from Captain's Mode

	 LUNA
>Moon Glaive bounces rebalanced to 1/2/3/9
>Moon Glaive damage reduction improved to 5%

      DRAGON KNIGHT
>Elder Dragon Form cooldown rebalanced to 110/90/70 seconds
>Elder Dragon From frost attack aoe increased from 250 to 350

	  CLOCKWERK
>Battery Assault attack interval rebalanced from 0.7 to 0.7/0.6/0.5/0.4 seconds

      DAZZLE
>Base Attack Time improved to 1.3 seconds

      LESHRAC
>Split Earth delay removed
>Diabolic Edict tower bonus increased to 80%
>Diabolic Edict damage increased to 9/18/27/36
>Lightning Storm damage increased to 100/200/300/400
>Lightning Storm slow duration increased to 1.5 seconds
>Lightning Storm slow increased to -100%
>Pulse Nova cooldown and manacost removed
>Pulse Nova mana cost per second rebalanced to 20
>Pulse Nova damage increased to 160/190/220
>Pulse Nove Aghanim's Scepter damage increased to 220/250/280
>Pulse Nova radius rebalanced to 450/550/650

       NATURE'S PROPHET
>Primary Attribute changed to Agility
>STR gain increased to 2.5
>AGI gain increased to 3.15
>Movespeed decreased to 290
>Teleportation manacost removed
>Teleportation cast point rebalanced to 3/2/2/1
>Teleportation cooldown rebalanced to 40/30/20/10
>Call of Nature treants increased to 5/6/7/8
>Call of Nature AoE changed to accomodate the previous change
>Wrath of Nature damage added increased to 11%

       LIFESTEALER
>Rage manacost removed
>Rage cooldown decreased to 14
>Rage duration rebalanced to 3/6/9/12
>Rage bonus attack speed rebalanced to 30/60/90/120
>Feast lifeleech increased to 10/15/20/25%
>Open Wounds now slows for -100% until the duration ends
>Open Wounds duration decreased to 6 seconds
>Infest cooldown rebalanced to 100/80/60

	DARK SEER
>Wall of Replica manacost rebalanced to 200
>Wall of Replica cooldown decreased from 100 to 100/80/60 seconds

	CLINKZ
>Strafe manacost reduced to 60
>Strafe bonus attackspeed increased to 130/160/190/220
>Searing Arrows manacost decreased to 5
>Searing Arrows damage increased to 50/60/70/80
>Skeleton Walk manacost decreased to 25
>Skeleton Walk movespeed bonus increased to 20/30/40/50%
>Death Pact duration increased to 60
>Death Pact health gain increased to 80%
>Death Pact damage gain increased to 8/10/12%
>Model size increased by 20%

	  OMNIKNIGHT
>Purify now works on spell immune allies
>Purify manacost rebalanced from 100/120/140/160 to 100
>Purify heal increased from 90/180/270/360 to 180/270/360/450
>Purify radius increased from 260 to 300
>Base damage increased by 10
>Base Attack Time improved from 1.7 to 1.5 seconds
>STR gain increased from 2.65 to 3.0
>AGI gain improved from 1.75 to 2.2

       JAKIRO
>Model size decreased by 30%

       ENCHANTRESS
>Untouchable slow increased to -400
>Untouchable slow duration rebalanced to 4/5/6/7
>Nature's Attendants count increased to [9/11]/13/15
>Impetus manacost removed
>Impetus Aghanim's Scepter bonus range increased to 290
>Armor reduced by 2

	  HUSKAR
>Inner Vitality heal increased from 2/4/6/8 to 8/12/16/20
>Inner Vitality attribute bonus heal increased from 5/10/15/20% to 20/30/40/50%
>Inner Vitality hurt attribute bonus heal increased from 30/45/60/75% to 60/75/95/110%
>Burning Spear damage increased from 5/10/15/20 to 15/20/25/30
>Berserker's Blood attack speed per stack increased from 14/16/18/28 to 28/20/22/24
>Berserker's Blood magic resistance per stack increased from 4/5/6/7 to 6/7/8/9
>Life Break Aghanim's Scepter damage increased from 65% to 75%
>Life Break Aghanim's Scepter cooldown reduced from 4 to 2

	NIGHT STALKER
>Hunter in the Night movespeed increased to 30/35/40/45%
>Hunter in the Night bonus attack speed increased to 30/60/90/120
>Darkness cast point rebalanced from 0.3 to 0.3/0.3/3.35
>Darkness cooldown reduced from 180/150/120 to 170/140/110
>Darkness duration increased from 40/60/80 to 60/80/100 seconds

	 BROODMOTHER
>Spawn Spiderlings count increased from 1/2/3/4 to 4/5/6/7
>Spin Web charge restore time reduced from 40 to 20 seconds
>Incapacitating Bite miss chance increased from 30/40/50/60 to 60/70/80/90
>Incapacitating Bite slow increased from 10/20/30/40% to 30/40/50/60%
>Incapacitating Bite duration increased from 2 to 2/4/6/8 seconds
>Base AGI increased from 18 to 26
>AGI gain increased from 2.2 to 2.7
>Movespeed increased from 295 to 310

	BOUNTY HUNTER
>Shurkien Toss manacost rescaled to 80
>Track bonus gold to teammates increased to 200/250/300
>Jinada cooldown improved from 12/10/8/6 to 10/8/6/4
>Jinada critical strike bonus damage increased from 150/175/200/225% to 200/250/300/350%

       WEAVER
>Sukuchi manacost removed
>The Swarm armor reduction increased from 1 to 1/2/3/4
>Geminate Attack cooldown reduced from 6/5/4/3 to 5/4/3/2

	  VIPER
>Poison Attack manacost removed
>Nethertoxin max damage increased from 40/80/120/160 to 80/120/160/200

	SPECTRE
>Spectral Dagger manacost rebalanced to 75
>Spectral Dagger damage type is now pure
>Spectral Dagger bonus movespeed increased to 10/20/30/40%
>Spectral Dagger damage increased to 100/200/300/400
>Desolate now ignores radius
>Desolate bonus damage increased to 60/70/80/90
>Desolate damage type is now physical
>Desolate now pierces spell immunity
>Dispersion damage reflected increased to 22/33/44/55%
>Haunt cooldown rebalanced to 100/80/60
>Spectre has been getting very fat lately so her movespeed is now 280

      CHEN
>Pentience move slow rebalanced from 14/18/22/26% to 10/20/30/40%
>Pentience bonus damage rebalanced from 14/18/22/26% to 10/20/30/40%
>Test of Faith damage no longer variable (it's always 100/200/300/400)

      DOOM BRINGER
>Devour manacost rebalanced to 60/50/40/30
>Devour bonus gold increased to 100/200/300/400
>Doom now has too much armor! (+1)
>Lvl? Death bonus damage increased from 20% to 40%
>Lvl? Death level multiple rebalanced from 6/5/4/3 to 4/3/2/1
>Doom cooldown reduced from 100 to 100/80/60 seconds

	ANCIENT APPARITION
>Ice Blast kill threshold increased to 10/15/20%

     SPIRIT BREAKER
>Greater Bash chance increased to 25/30/35/40%
>Charge of Darkness cast point removed (was 0.47)
>Charge of Darkness movespeed increased from 600/650/700/750 to 700/750/800/850
>Charge of Darkness stun duration increased from 1.2/1.6/2.0/2.4 to 2.0/2.4/2.8/3.2
>Nether Strike cooldown rebalanced to 80/60/40
>Empowering Haste bonus movespeed increased from 6/10/14/18% to 10/14/18/22%
>Empowering Haste active duration increased to 15 seconds
>Empowering Haste active bonus movespeed increased to +5/7/9/11%
>Base Attack Time improved to 1.7 seconds

     URSA WARRIOR
>Fury Swipes damage increased to 30/40/50/60
>Main attribute changed to Strength
>STR gain increased to 3.0

      GYROCOPTER
>Flak Cannon cooldown rebalanced to 30/30/30/15
>Flak Cannon duration increased to 15
>Flak Cannon max attacks increased to 6/9/12/15
>Rocket Barrage rockets per second increased from 10 to 40

      INVOKER
>Ghost Walk manacost and cooldown removed
>Tornado lift duration increased to 9.5 seconds
>EMP delay removed
>EMP AoE increased from 675 to 875
>EMP damage per mana burned increased from 50% to 60%

      SILENCER
>Curse of the Silent cast point removed
>Curse of the Silent cooldown reduced from 20/16/12/8 to 16/12/8/4
>Curse of the Silent manacost rebalanced to 75
>Curse of the Silent mana drain increased from 8/16/24/32 to 24/32/40/48
>Glaives of Wisdom manacost rebalanced from 15 to 15/10/5/0
>Glaives of Wisdom damage from int increased from 30/48/66/84% to 30/60/90/120%
>Last Word damage now pure
>Last Word cooldown reduced from 36/18/20/12 to 28/20/12/6 seconds
>Global Silence manacost rebalanced from 250/375/500 to 200

      LYCANTHROPE
>Shapeshift duration rebalanced to 18/24/30
>For 1.4 seconds before Shapeshift finishes it can be interrupted
>Shapeshift critical strike damage increased to 170/200/230%

      CHAOS KNIGHT
>Critical Strike chance increased to 30/40/50/60%
>Chaos Bolt manacost rebalanced to 140/70/35/0
>Chaos Bolt minimum stun duration rebalanced to 1/2/3/4
>Chaos Bolt maximum stun duration rebalanced to 2/3/4/5
>Phantasm cooldown reduced to 120/100/80

      OUTWORLD DEVOURER
>Astral Imprisonment cooldown rebalanced from 18/16/14/12 to 16/14/12/10
>Astral Imprisonment manacost rebalanced from 120/140/160/180 to 100
>Astral Imprisonment int steal increased to 6/8/10/12
>Astral Imprisonment steal duration increased from 60 to 60/70/80/90
>Essence Aura restore chance increased from 40% to 40/50/60/70%

      TREANT PROTECTOR
>Leech Seed slow increased from 28% to 80%

      RUBICK
>Telekinesis now does 100/150/200/250 damage (magical)

      NAGA SIREN
      MEEPO
>Naga Siren's Ensnare and Meepo's Earthbind have switched places
      
      KEEPER OF THE LIGHT
>Chakra Magic manacost removed
>Chakra Magic restore increased to 150/225/300/375

      GUARDIAN WISP
>Tether heal amplification doubled
>Tether latch range increased from 1800 to 1800/2000/2200/2400
>Tether bonus movespeed increased from 17% to 22%
>STR gain increased to 2.9
>Overcharge bonus attack speed rebalanced from 40/50/60/70 to 30/60/90/120
>Overcharge damage reduction improved from 5/10/15/20% to 10/15/20/25%
>Overcharge health and mana drain percentage rebalanced from 4.5% to 0.5%
>Armor increased by 2

      BRISTLEBACK
>Bristleback side damage reduction increased to 24/32/40/48%
>Bristleback back damage reduction increased to 24/32/40/48%
>Vicious Nasal Goo armor reduction per stack increased from 1/1/2/2 to 1/1/2/3

      SLARK
>Pounce manacost removed
>Pounce cooldown reduced from 20/16/12/8 to 16/12/8/6 seconds
>Pounce damage increased from 60/120/180/240 to 120/180/240/300
>Essence Shift stat loss and agi gain doubled
>Essence Shift duration rebalanced from 15/30/60/120 to 30/60/90/120

      MAGNUS
>Skewer cooldown rebalanced from 30 to 30/26/22/18 seconds
>Skewer manacost removed
>Skewer speed increased from 950 to 1200
>Skewer radius increased from 125 to 200
>Reverse Polarity manacost rebalanced from 200/250/300 to 200

      TIMBERSAW
>Whirling Death stat loss percentage increased from 15% to 35%
>Reactive Armor bonus armor increased from 1 to 2
>Reactive Armor bonus regen increased from 1 to 3
>Reactive Armor stack duration increased from 16 to 16/20/24/28 seconds
[NOTE: Chakram changes affect both regular and Aghanim's Scepter Chakram]
>Chakram pass damage incresed from 100/140/180 to 140/180/220
>Chakram damage per second increased from 50/75/100 to 50/100/150

      ABBADAON
>Curse of Avernus slow increased from 5/10/15/20% to 10/15/20/25%
>Curse of Avernus bonus attack speed increased from 10/20/30/40 to 30/50/70/90

      ELDER TITAN
>Natural Order magic resistance reduction increased from 12/19/26/33% to 19/26/33/40%

      TUSK
>Walrus Punch critical damage increased to 350/500/650%
>Walkrus Punch <50% HP critical damage increased to 400/550/700%

	SKYWRATH MAGE
>Arcane Bolt cooldown increased to 5 seconds
>Ancient Seal manacost rebalanced from 80/90/100/110 to 80
>Ancient Seal magic resistance reduction increased from 30/35/40/45% to 50%

	LEGION COMMANDER
>Overwhelming odds cooldown rebalanced to 15 seconds
>Overwhelming odds damage now pure
>Overwhelming odds radius increased from 330 to 400
>Overwhelming odds base damage increased from 40/80/120/160 to 120/160/200/240
>Overwhelming odds bonus speed increased to 9% (per creep) and 18% (hero)
>Overwhelming odds duration increased from 7 to 15 seconds
>Press the Attack manacost decreased to 60
>Press the Attack duration increased to 10
>Press the Attack bonus attack speed increased to 60/90/120/150
>Moment of Courage cooldown removed
>Moment of Courage trigger chance increased to 20/30/40/50%
>Moment of Courage lifesteal increased to 40/60/80/100%
>Duel duration increased to 5.5/6.0/6.5
>Duel cooldown rebalanced to 50/30/10
>Duel reward damage increased to 18/26/32
>Base Attack Time improved to 1.6 seconds

      EMBER SPIRIT
>Searing Chains duration rebalanced to 1/2/3/4
>Searing Chains count increased to 5
>Searing chains total damage increased to 80/200/360/560
>Sleight of Fist cooldown reduced to 15/11/7/3
>Sleight of Fist bonus damage to creeps changed from -50% to +100%
>Fire Remnant charges increased from 3 to 3/4/5
>Fire Remnant restore time improved to 15 seconds
>Activate Fire Remnant manacost removed

	
      EARTH SPIRIT
>Added to Captain's Mode
>Magnetize damage per second increased to 50/75/100
>Magnetize now pierces spell immunity
>Magnetize damage is now pure
>Earth Spirit's Stone Remnant now has a 0.1 second cooldown
>Stone remnant recharge time reduced from 30 to 15 seconds

      TERRORBLADE
>Reflection cast range increased to 650
>Reflection manacost removed
>Relfection cast point decreased to 0.0
>Reflection damage rebalanced to 100%
>Reflection move slow rebalanced to 100%
>Conjure Image manacost removed
>Conjure Image outgoing damage rebalanced to 70/80/90/100%
>Conjure Image incoming damage rebalanced to 100/100/100/70%
>Metamorphosis cooldown reduced to 100 seconds
>Metamorphos is now Terrorblade's ultimate
>Sunder is now a regular ability
>Sunder cooldown is now 160/120/80/40
>Sunder manacost is now 250/200/100/0
>Sunder hit point minimum percentage is now 25/20/15/10%
>Base strength increased to 22
>STR gain increased to 1.6
>AGI gain increased to 3.5
>Magic resistance increased to 35%

      PHOENIX
>Icarus Dive cooldown rebalanced from 36 to 36/32/28/24 seconds
>Icarus Dive damage improved from 10/30/50/70 to 30/50/70/90
>Supernova attacks to destroy increased from 5/7/10 to 7/10/13

      ORACLE
>Purifying Flames manacost rebalanced from 55/70/85/100 to 55
>False Promise manacost rebalanced from 200 to 200/100/0
>Added to Captain's Mode

 * ITEMS

	BLINK DAGGER
>Cost decreased to 1800
>Damage cooldown decreased to 1
>Range increased to 2400

      QUELLING BLADE
>Cost decreased to 100
>Cooldown removed
>Melee bonus increased to 50%
>Ranged bonus increased to 40%

     STOUT SHIELD
>Cost is now 50
>Damage block is now 30 (melee) and 20 (ranged)
>Block chance increased from 60% to 80%

       GLOVES OF HASTE
>Attack speed increased to 30

	BOOTS OF TRAVEL
>Movespeed increased from 100 to 120
>Manacost removed

	PHASE BOOTS
>Phase duration increased from 4 to 6 seconds

	BLOODSTONE
>Initial charges increased from 8 to 10

	EAGLESONG
>Cost reduced to 2800

	MEKANISM
>Recipie cost reduced from 900 to 300
>Cooldown reduced from 45 to 30 seconds

	VLADIMIR'S OFFERING
>Lifesteal increased from 16% to 20%
>Damage increased from 15% to 20%

	FLYING COURIER
>Cost reduced to 10
>Now stocked initially

	RING IF BASILIUS
>Bonus armor increased to 2 (total of 4)

	EUL'S SCEPTER OF DIVINITY
>INT bonus increased to 20
>Bonus movespeed increased to 50
>Bonus mana regen increased to 250%

       FORCE STAFF
>Recepie cost decreased to 100 (1450)

	 EYE OF SKADI
>Stat bonus increased to 30
>Move slow increased to -55%
>Attack slow increased to -65

	BLACK KING BAR
>Recepie cost decreased to 1000 (3600)
>Bonus strength increased to 15
>Bonus damage increased to 30

	DIVINE RAPIER
>Bonus damage increased from 300 to 400

	RADIANCE
>Recepie cost reduced to 900
>Burn radius increased from 700 to 900
>Burn damage increased from 50 to 70

       AEGIS OF THE IMMORTAL
>Can now be dropped
>Cannot be shared

	RING OF HEALTH
>Cost decreased to 675

      VOID STONE
>Cost decreased to 675

      BATTLE FURY
>Components changed. New recepie: Claymore, Perseverence
>New cost 2750
>Bonus damage increased to 70
>HP regen increased to 8
>Mana regen increased to 175%
>Cleave increased to 45%
>Added damage block for melee heroes - 100% chance to block 30 damage

       SANGE
>Recepie cost decreased to 100
       YASHA
>Recepie cost decreased to 100

	HAND OF MIDAS
>Cast range now global
>Cooldown decreased to 40
>Recepie cost decreased to 500
>Bonus attack speed increased to 60
>XP multiplier increased to 10x
>Bonus gold increased to 500

       MAELSTROM
>Recepie cost decreased to 100
>Attack speed increased to 30

       MJOLLNIR
>Attack speed increased to 100
>Recepie cost decreased to 100

       ASSAULT CUIRASS
>Recepie cost decreased to 300
>Bonus attack speed increased to 55
>Armor aura increased to 10 (both positive and reduction)
>Attack speed aura increased to 25

	SHADOW BLADE
>Manacost removed
>Cooldown reduced from 28 to 20 seconds

	DESOLATOR
>Recipie cost decreased from 900 to 300
>Armor reduction increased from 7 to 16


	MASK OF MADNESS
>Extra damage taken decreased from 30% to 20%
>Berserk attack speed increased from 100 to 160

       HEART
>Heal increased to 4%
>Recepie cost decreased to 1000

	BUTTERFLY
>Cooldown decreased from 35 to 15 seconds
>Bonus AGI increased from 30 to 35
>Evasion increased from 35% to 45%
>Attack speed increased from 30 to 60
>Flutter move speed bonus increased from 20% to 30%